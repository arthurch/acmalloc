#ifndef TEST_MALLOC_H
#define TEST_MALLOC_H

#include <stdint.h>
#include <stddef.h>

#define USE_AC_PREFIX
#define AC_USE_UNISTD 1

#define AC_DEBUG_LOG 0
/* provide a function to dump heap structure */
// #define AC_HEAP_DUMP

/****************************************/

/* it talks a lot so use it at your own risks ! */
// #define AC_VEBOSE_MODE

#define AC_MAGIC 0xc0dedeee
#define AC_DEFAULT_HEAP_SIZE 8192
#define AC_MINIMUM_EXPAND_SIZE 512

/* printf function */
#define ac_log_print printf

void acmalloc_init(uintptr_t heap_start, size_t heap_size);

__attribute__((malloc)) void *acmalloc(size_t sz);
__attribute__((malloc)) void *accalloc(size_t nitems, size_t size);
__attribute__((malloc)) void *acrealloc(void *ptr, size_t size);
void acfree(void *ptr);

#ifdef AC_HEAP_DUMP
void heap_struct_dump();
#endif

#ifndef USE_AC_PREFIX
#define malloc acmalloc
#define realloc acrealloc
#define calloc accalloc
#define free acfree
#endif

#endif
