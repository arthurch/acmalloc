/*
 * ac memory allocator
 *
 * Copyright (C) 2020 arthurc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * About acmalloc
 * """"""""""""""
 *
 * acmalloc was inspired by Doug Lea's memory allocator
 * (dlmalloc). It's a simple high level memory management
 * algorithm providing standard malloc, realloc and free
 * functions. It basically just need a heap (and a proper
 * way to expand it) to work fine (well maybe not
 * that fine for now but it's a beginning).
 *
 * How it works
 * """"""""""""
 *
 * acmalloc handles memory as chunks of different sizes.
 * These chunks are either currently used by the program
 * that allocated them or free and stored in one of the
 * several linked list.
 * Chunks have a size which is always a multiple of 8 and
 * are 8-bit aligned.
 * Minimum size is 16 bytes on a 32 bit system and 32 bytes
 * on a 64 bit system (in theory, in reality it depends on
 * the implementation of size_t and uintptr_t of your system).
 *
 * - Chunk structure
 * free chunk :
 *   prev_size (size of previous chunk)
 *   size + flags (here : AC_FLAG_FREE)
 *   fwd (pointer to next free chunk)
 *   bwd (pointer to previous free chunk)
 *   ... (actual chunk memory)
 *   size (which will be the "prev_size" member of the following chunk in memory)
 * used chunk :
 *   prev_size (size of previous chunk)
 *   size + flags (here : AC_FLAG_USED)
 *   ... (used memory)
 *   size (which will be the "prev_size" member of the following chunk in memory)
 *
 * - Linked lists
 * Two types of linked lists :
 *  -> chunks below 512 bytes are stored in "small" lists,
 *  which are lists of same size chunks. There are 64
 *  small lists (multiple of 8 from 16 to 504)
 *  -> chunks above or equals to 512 bytes are stored in
 *  "large" lists sorted by size. There are 24 large lists
 *  (power of 2 from 2^9 to 2^31). Each large list stores
 *  chunk with a size greater or equals to its size.
 *
 * Things we can improve
 * """""""""""""""""""""
 *
 * todo: provide a way to allocate alligned memory
 * todo: make chunks coalesce together
 * todo: make it thread safe
 */

/* this is probably wrong (edit it if necessary) */
#include "malloc.h"

#ifdef AC_DEBUG_LOG
#include <stdio.h>
#endif

#ifdef AC_USE_UNISTD
#include <unistd.h>
#endif

#define AC_SIZE_T_SIZE (__SIZEOF_SIZE_T__)

/*
 * GCC attribute
 */
#define alwaysinline inline __attribute__((always_inline))

/*
 * Type representing chunk pointers
 */
typedef uintptr_t mchunkptr;

/*
 * Chunk flags
 */
#define AC_FLAG_FREE (0x1)
#define AC_FLAG_USED (~0x1)
#define AC_FLAG_MMAP (0x2)
#define AC_FLAG_UNUSED (0x4)
/*
 * Chunk flags masks
 */
#define AC_SIZE_MASK (~0x7)
#define AC_FLAGS_MASK (0x7)

/*
 * Structure representing a free chunk
 */
struct mfreechunk {
  size_t prev_size;
  size_t size; /* bits 0-2 are flags */
  /* bit 0 : status (0 if used, 1 if free), bit 1 : mmaped (0 if heap chunk,
   1 if mmaped chunk), bit 2 : unused */
  mchunkptr fwd;
  mchunkptr bwd;
};
typedef struct mfreechunk mfreechunk_t;

#define M_SMALL_LISTS_COUNT 64
#define M_SMALLCHUNKS_MAX 504
#define M_LARGE_LISTS_COUNT 24

#define M_MIN_CHUNK_SIZE (2*AC_SIZE_T_SIZE + 2*sizeof(mchunkptr))

/*
 * Heap informations
 */
struct heap_state {
  uint32_t magic;

  /* informations about heap */
  uintptr_t heap_start;
  uintptr_t heap_size;

  /* lists containing free chunks */
  mchunkptr small_lists[M_SMALL_LISTS_COUNT];
  mchunkptr large_lists[M_LARGE_LISTS_COUNT];
} __attribute__((aligned (8)));

/* current heap state (should never be accessed directly) */
static struct heap_state _hs;
/* actual macro to access heap state */
#define get_heap_state() (&_hs)

/************** UTILITIES *******************/

static void *acmemset(void *dest, int val, size_t len)
{
  unsigned char *ptr = (unsigned char *) dest;
  while (len-- > 0)
    *ptr++ = val;
  return dest;
}

static void *acmemcpy(void* s1, const void* s2, size_t n)
{
  char *cdest;
  char *csrc;
  unsigned int *ldest = (unsigned int *) s1;
  unsigned int *lsrc = (unsigned int *) s2;

  while(n >= sizeof(unsigned int))
  {
    *ldest++ = *lsrc++;
    n -= sizeof(unsigned int);
  }

  cdest = (char *) ldest;
  csrc = (char *) lsrc;

  while(n > 0)
  {
    *cdest++ = *csrc++;
    n -= 1;
  }

  return s1;
}
/* math utilities */

#define M_ALIGN_8(X) (((X) + 7) & ~7)

/*
 * Get the index of the linked list corresponding to the size sz
 * (for large chunks only)
 * @return the index
 */
static alwaysinline size_t ac_large_index(size_t sz)
{
  return 32 - __builtin_clz(sz) - 10;
}

/* linked list utilities */

/*
 * Add a chunk after the given element
 */
static alwaysinline void list_add_after(mfreechunk_t *after, mchunkptr src)
{
  mfreechunk_t *fwd = (mfreechunk_t *) after->fwd;
  if(fwd != 0)
  {
    fwd->bwd = src;
  }
  mfreechunk_t *curr = (mfreechunk_t *) src;
  curr->fwd = (mchunkptr) fwd;
  curr->bwd = (mchunkptr) after;
  after->fwd = src;
}

/*
 * Add a chunk before the given element
 */
static alwaysinline void list_add_before(mfreechunk_t *before, mchunkptr src, mchunkptr *list_start)
{
  mfreechunk_t *bwd = (mfreechunk_t *) before->bwd;
  if(bwd == 0)
  {
    *list_start = src;
  } else
  {
    bwd->fwd = src;
  }
  mfreechunk_t *curr = (mfreechunk_t *) src;
  curr->fwd = (mchunkptr) before;
  curr->bwd = (mchunkptr) bwd;
  before->bwd = src;
}

/*
 * Add a chunk at the beginning of the list
 */
static alwaysinline void list_add_first(mchunkptr *list_start, mfreechunk_t *src)
{
  if(*list_start == 0)
  {
    src->fwd = 0;
    src->bwd = 0;
    *list_start = (mchunkptr) src;
  } else
  {
    mfreechunk_t *first = (mfreechunk_t *) *list_start;
    first->bwd = (mchunkptr) src;
    src->bwd = 0;
    src->fwd = (mchunkptr) first;
    *list_start = (mchunkptr) src;
  }
}

/*
 * Remove a chunk from the linked list
 */
static alwaysinline void list_remove_me(mfreechunk_t *me, uintptr_t *list_start)
{
  if(me->bwd == 0)
  {
    /* it's the first element of the list */
    *list_start = (mchunkptr) me->fwd;
  } else
  {
    mfreechunk_t *bwd = (mfreechunk_t *) me->bwd;
    bwd->fwd = me->fwd;
  }

  if(me->fwd != 0)
  {
    mfreechunk_t *fwd = (mfreechunk_t *) me->fwd;
    fwd->bwd = me->bwd;
  }
}

/****** internal functions ******/
static int acmalloc_begin(struct heap_state *hs);
static void storechunk(struct heap_state *hs, mfreechunk_t *free, size_t sz);
static mchunkptr getchunk(struct heap_state *hs, size_t sz, int index);
static mchunkptr no_memory_left(size_t sz);

/***** malloc ******/

/*
 * malloc algorithm :
 *
 * small chunk : look inside the linked list corresponding to the
 *  size we want. If a chunk is ready, use it. If not, look if
 *  a greater chunk is ready, then split it in two separate chunks.
 *  If not, redo it until we found an available chunk
 *
 * large chunk : look inside the linked list corresponding to the
 *  size we want : loop through all free chunks and pick the best
 *  one, then split it if necessary. If no chunks are available,
 *  look inside the linked list at index+1 until we find a larger
 *  chunk that we can split.
 */

/*
 * Split a large chunk into two smaller one (the first with
 * requested size and the second with remaining size)
 *
 * CHUNK NEEDS TO BE REMOVED FROM THE LINKED LIST BEFORE
 * CALLING THIS FUNCTION
 *
 * @return chunk of requested size (with AC_FLAG_USED set)
 */
static mchunkptr splitlargechunk(struct heap_state *hs, mfreechunk_t *chunk, size_t sz)
{
  size_t csz = chunk->size & AC_SIZE_MASK;
  if(csz < sz || csz == sz) /* we can't do much in these cases */
    return (mchunkptr) chunk;

  size_t rsz = csz - sz; /* remaining size */
  if(rsz < M_MIN_CHUNK_SIZE)
  {
    /* if remaining size is lesser than the minimum size,
      we just do nothing */
    return (mchunkptr) chunk;
  }

  mfreechunk_t *part1 = chunk;
  mfreechunk_t *part2 = (mfreechunk_t *) ((mchunkptr)chunk + sz);

  part1->size = sz & AC_FLAG_USED; /* first part is set to USED */
  *((size_t *) ((mchunkptr)part1 + sz)) = sz;
  part2->size = rsz | AC_FLAG_FREE; /* remaining part is set to FREE */
  *((size_t *) ((mchunkptr)part2 + rsz)) = rsz;

  storechunk(hs, part2, rsz); /* store the remaining part */

  return (mchunkptr) part1;
}

/*
 * Get a chunk from one of the linked lists (index parameter
 * is used by the function itself so just set it to 0 and it
 * should be fine)
 *
 * @return a usable chunk of requested size (with AC_FLAG_USED
 * set)
 */
static mchunkptr getchunk(struct heap_state *hs, size_t sz, int index)
{
  if(sz < M_MIN_CHUNK_SIZE)
  {
    return 0;
  }

  if(sz < M_SMALLCHUNKS_MAX)
  {
    /* small chunk */

    /* search if a chunk of requested size is available */
    size_t index = sz / 8;
    if(hs->small_lists[index] != 0)
    {
      mfreechunk_t *chunk = (mfreechunk_t *) hs->small_lists[index];
      list_remove_me(chunk, &(hs->small_lists[index]));
      chunk->size &= AC_FLAG_USED;

      return (mchunkptr) chunk;
    }

    /* if not, split a larger one recursively */
    mchunkptr c = getchunk(hs, sz * 2, 0);
    if(c == 0)
      return 0;
    /* split that chunk in two parts of the same size */
    mfreechunk_t *half1 = (mfreechunk_t *) c;
    mfreechunk_t *half2 = (mfreechunk_t *) (c + sz);
#ifdef AC_VEBOSE_MODE
    ac_log_print("splitting small chunk (s=%zu), half1=%p, half2=%p\n", sz, half1, half2);
#endif
    /* set new size and flags */
    half1->size = sz & AC_FLAG_USED; /* first half is set to USED */
    *((size_t *) ((mchunkptr)half1 + sz)) = sz;
    half2->size = sz | AC_FLAG_FREE; /* second half is set to FREE (as it remains a free chunk) */
    *((size_t *) ((mchunkptr)half2 + sz)) = sz;

    /* add second half to the list */
    half2->bwd = 0;
    half2->fwd = 0;
    list_add_first(&(hs->small_lists[index]), half2);

    /* return first half */
    return (mchunkptr) half1;
  } else
  {
    /* large chunk */

    if(__builtin_expect(index >= M_LARGE_LISTS_COUNT, 0)) /* not supposed to happen */
    {
      return 0;
    }
    if(index <= 0)
    {
      /* if index is smaller or equal to 0, we set it to the index
        corresponding to the size we want */
      index = ac_large_index(sz);
    }

    mfreechunk_t *chunk = (mfreechunk_t *) hs->large_lists[index];
    while(chunk != 0)
    {
      if((chunk->size & AC_SIZE_MASK) >= sz)
      {
#ifdef AC_VEBOSE_MODE
        ac_log_print("found a larger chunk %zu (%p, index=%i)\n", chunk->size & AC_SIZE_MASK, chunk, index);
#endif
        /* we found a larger chunk, split it and we are fine
          (don't forget to remove it from the list before) */
        list_remove_me((mfreechunk_t *) chunk, &(hs->large_lists[index]));
        return splitlargechunk(hs, (mfreechunk_t *) chunk, sz);
      }
      chunk = (mfreechunk_t *) chunk->fwd;
    }

    size_t next_index = index + 1;
    if(next_index >= M_LARGE_LISTS_COUNT)
    {
#ifdef AC_VEBOSE_MODE
      ac_log_print("looks like we ran out of memory, give me some more (sz=%zu)\n", sz);
#endif
      return no_memory_left(sz);
    }
    /* if no chunks of this size are available, try to find
      a larger one recursively */
    mchunkptr c = getchunk(hs, sz, next_index);
    if(c == 0)
      return 0;
    /* we now have a larger chunk, split it */
    mchunkptr split = splitlargechunk(hs, (mfreechunk_t *) c, sz);
    return split;
  }
}

/*
 * Actual malloc function
 *
 * @return a pointer to the allocated memory
 */
__attribute__((malloc)) void *acmalloc(size_t _size)
{
  struct heap_state *hs = get_heap_state();
  if(hs->magic != AC_MAGIC)
  {
    /* acmalloc not initialized */
    if(acmalloc_begin(hs) == -1)
      return (void *) 0;
  }

  size_t sz = _size + 2*AC_SIZE_T_SIZE;

#if AC_DEBUG_LOG == 1
  ac_log_print("acmalloc: allocating chunk : wanted size=%zu, effective size=%zu\n", _size, M_ALIGN_8(sz));
#endif
  sz = M_ALIGN_8(sz);

  /* this function do all the job for us */
  mchunkptr chunk = getchunk(hs, sz, 0);

#ifdef AC_VEBOSE_MODE
  ac_log_print("acmalloc: just allocated 0x%zx (s=%zu)\n", chunk, ((mfreechunk_t *) chunk)->size);
#endif
  if(chunk == 0)
    return (void *) 0;
  return (void *) (chunk + 2*AC_SIZE_T_SIZE);
}

/*
 * calloc function
 * The difference between malloc and calloc is
 * that calloc sets allocated memory to zero.
 *
 * @return a pointer to the allocated memory (set to zero)
 */
__attribute__((malloc)) void *accalloc(size_t nitems, size_t size)
{
  void *ptr = (void *) acmalloc(nitems * size);
  acmemset(ptr, 0, nitems * size);

  return ptr;
}

/*
 * realloc function
 * It attempts to resize the memory block pointed by ptr
 * that was previously allocated using malloc or calloc.
 *
 * @return a pointer to the newly allocated memory
 */
__attribute__((malloc)) void *acrealloc(void *ptr, size_t size)
{
  /*
  * C standard implementation: When NULL is passed to realloc,
  * simply malloc the requested size and return a pointer to that.
  */
  if(__builtin_expect(ptr == NULL, 0))
    return acmalloc(size);

  /*
  * C standard implementation: For a size of zero, free the
  * pointer and return NULL, allocating no new memory.
  */
  if(__builtin_expect(size == 0, 0))
  {
    acfree(ptr);
    return NULL;
  }

  struct heap_state *hs = get_heap_state();
  if((((mchunkptr) ptr) < hs->heap_start) || (((mchunkptr) ptr) > (hs->heap_start + hs->heap_size)))
  {
    /* this address is not in the heap */
#if AC_DEBUG_LOG == 1
    ac_log_print("acmalloc: [WARNING] %p is not in the heap\n", ptr);
#endif
    return NULL;
  }

  /* we need to get the size of this chunk */
  mfreechunk_t *chunk = (mfreechunk_t *) ((mchunkptr)ptr - 2*AC_SIZE_T_SIZE);
  size_t cpy_size = chunk->size & AC_SIZE_MASK;

  cpy_size = cpy_size - 2*AC_SIZE_T_SIZE;

  if(cpy_size == size)
    return ptr;

  /* allocating new memory */
  void *new_ptr = (void *) acmalloc(size);

  /* if the new memory block is smaller, be sure to copy
    not too much memory */
  if(cpy_size > size)
    cpy_size = size;
  /* copy memory */
  acmemcpy(new_ptr, ptr, cpy_size);

  /* get rid of the old memory */
  acfree(ptr);

  return new_ptr;
}

/***** free ******/

/*
 * Store the chunk in one of the linked list
 * (chunk is then supposed to be usable)
 */
static void storechunk(struct heap_state *hs, mfreechunk_t *free, size_t sz)
{
  if(sz < M_MIN_CHUNK_SIZE)
  {
    /* we can't handle chunks too small */
    return;
  }

  if(sz <= M_SMALLCHUNKS_MAX)
  {
    /* small chunk */

    size_t index = sz / 8;
    free->bwd = 0;
    free->fwd = 0;
    list_add_first(&(hs->small_lists[index]), free); /* easy right ? */
  } else
  {
    /* large chunk */

    /* this case is harder because we have to loop through
      the linked list to find a place for this chunk as
      they are sorted by size */
    size_t index = ac_large_index(sz);
    size_t s;

    mfreechunk_t *chunk = (mfreechunk_t *) hs->large_lists[index];
    while(chunk != 0)
    {
      s = chunk->size & AC_SIZE_MASK;
      if(s == sz)
      {
        /* if there is already a chunk of the same size,
          just add ours after it */
        list_add_after((mfreechunk_t *) chunk, (mchunkptr) free);
        return;
      } else if(s > sz)
      {
        /* if we found a larger chunk, we add ours before */
        list_add_before((mfreechunk_t *) chunk, (mchunkptr) free, &(hs->large_lists[index]));
        return;
      }
      chunk = (mfreechunk_t *) chunk->fwd;
    }
    /* if we reach that point, it means that list is empty */
    list_add_first(&(hs->large_lists[index]), free);
    return;
  }
}

void acfree(void *ptr)
{
  struct heap_state *hs = get_heap_state();
  if(hs->magic != AC_MAGIC)
  {
    /* acmalloc not initialized */
    if(acmalloc_begin(hs) == -1)
      return;
  }

  if((((mchunkptr) ptr) < hs->heap_start) || (((mchunkptr) ptr) > (hs->heap_start + hs->heap_size)))
  {
    /* this address is not in the heap */
#if AC_DEBUG_LOG == 1
    ac_log_print("acmalloc: [WARNING] %p is not in the heap\n", ptr);
#endif
    return;
  }

  mfreechunk_t *free = (mfreechunk_t *) ((mchunkptr)ptr - 2*AC_SIZE_T_SIZE);

  free->size = free->size | AC_FLAG_FREE;

  size_t sz = free->size & AC_SIZE_MASK;
#if 0
  if(free->size & AC_FLAG_MMAP)
  {
    /* chunk was allocated using mmap */
    /* TODO : munmap */
    return;
  }
#endif

#if AC_DEBUG_LOG == 1
  ac_log_print("freeing chunk %p with size = %zu, index=%zu (%s chunk), flags=0x%zx\n", free, free->size & AC_SIZE_MASK,
    (sz < M_SMALLCHUNKS_MAX) ? (sz/8) : (ac_large_index(sz)), (sz < M_SMALLCHUNKS_MAX) ? "small" : "large", free->size & AC_FLAGS_MASK);
#endif

  storechunk(hs, free, sz);
}

#ifdef AC_HEAP_DUMP
/*
 * Dump all linked lists (DEBUG)
 */
void heap_struct_dump()
{
  struct heap_state *hs = get_heap_state();

  ac_log_print("acmalloc: Heap structure dump\n");
  ac_log_print("acmalloc: Heap start : 0x%zX\n", hs->heap_start);
  ac_log_print("acmalloc: Heap size : %zu\n", hs->heap_size);

  size_t i;

  /* dump small chunks */
  ac_log_print("acmalloc: small chunks : \n");
  for(i = 2; i<M_SMALL_LISTS_COUNT; i++)
  {
    ac_log_print("acmalloc: - list %zu, size = %zu", i, i*8);
    mfreechunk_t *curr = (mfreechunk_t *) hs->small_lists[i];
    while(curr != 0)
    {
      ac_log_print(" -> %p", curr);
      curr = (mfreechunk_t *) curr->fwd;
    }
    ac_log_print("\n");
  }

  /* dump large chunks */
  ac_log_print("acmalloc: large chunks : \n");
  for(i = 0; i<M_LARGE_LISTS_COUNT-1; i++)
  {
    ac_log_print("acmalloc: - list %zu, size = %u", i, (1 << (i+9))); /* 2^(i+9) */
    mfreechunk_t *curr = (mfreechunk_t *) hs->large_lists[i];
    while(curr != 0)
    {
      ac_log_print(" -> %p:%zu", curr, curr->size & AC_SIZE_MASK);
      curr = (mfreechunk_t *) curr->fwd;
    }
    ac_log_print("\n");
  }
}
#endif

/*
 * Initialize acmalloc (just give us a heap and
 * we are good to go !)
 */
void acmalloc_init(uintptr_t heap_start, size_t heap_size)
{
  struct heap_state *hs = get_heap_state();

  hs->magic = AC_MAGIC;
  hs->heap_start = heap_start;
  hs->heap_size = heap_size;

  acmemset((void *) hs->small_lists, 0, sizeof(mchunkptr) * M_SMALL_LISTS_COUNT);
  acmemset((void *) hs->large_lists, 0, sizeof(mchunkptr) * M_LARGE_LISTS_COUNT);

  /* initialize heap */
  mfreechunk_t *heap = (mfreechunk_t *) heap_start;
  heap->prev_size = 0;
  size_t usable_size = heap_size - 2*AC_SIZE_T_SIZE;
  heap->size = usable_size | AC_FLAG_FREE;
  storechunk(hs, heap, usable_size);
}

/*************************************/
/******* heap creation routine *******/
/*************************************/

/*
 * Provide a heap using standard functions
 * and initialize acmalloc with it
 *
 * @return 0 on success, -1 if an error occured
 */
static int acmalloc_begin(struct heap_state *hs)
{
  if(hs->magic == AC_MAGIC) /* already initialized */
    return 0;

  void *heap_start = (void *) 0;

  /*** create heap (using system dependant functions) ***/

#ifdef AC_USE_UNISTD

  heap_start = (void *) sbrk(0);
  if(heap_start == (void *)-1)
  {
#if AC_DEBUG_LOG == 1
    printf("acmalloc: [ERROR] failed to get current program break\n");
#endif
    return -1;
  }
  if(sbrk(AC_DEFAULT_HEAP_SIZE) == (void *)-1)
  {
#if AC_DEBUG_LOG == 1
    printf("acmalloc: [ERROR] failed to increase program break\n");
#endif
    return -1;
  }

#endif

  /*** initialize acmalloc ***/
  acmalloc_init((uintptr_t) heap_start, AC_DEFAULT_HEAP_SIZE);

  return 0;
}

/**************************************/
/******* no memory left routine *******/
/**************************************/

/*
 * Expand the heap if we need more memory
 *
 * @return a chunk of requested size which is
 * supposed to be directly usable (set to AC_FLAG_USED)
 */
static mchunkptr no_memory_left(size_t sz)
{
  if(sz < AC_MINIMUM_EXPAND_SIZE)
    sz = AC_MINIMUM_EXPAND_SIZE;

  /*** expand heap (using system dependant functions) ***/

#ifdef AC_USE_UNISTD

#if AC_DEBUG_LOG == 1
  ac_log_print("acmalloc: expanding heap: %zu (current heap size: %zu)\n", sz, get_heap_state()->heap_size);
#endif
  if(sbrk(sz) == (void *)-1)
  {
#if AC_DEBUG_LOG == 1
    printf("acmalloc: [ERROR] failed to expand heap\n");
#endif
    return 0;
  }

#endif

  /*** update heap state and return the chunk ***/

  struct heap_state *hs = get_heap_state();
  uintptr_t heap_end = hs->heap_start + hs->heap_size;
  hs->heap_size += sz;

  mfreechunk_t *c = (mfreechunk_t *) (heap_end - AC_SIZE_T_SIZE);
  c->size = sz & AC_FLAG_USED;

  return (mchunkptr) c;
}
