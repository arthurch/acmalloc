#include <stdlib.h>
#include <stdio.h>

#include "../malloc.h"

extern int malloc_test(int verbose);

int main(int argc, char **argv)
{
  int verbose = 1;

  printf("memory testing application\n");

  malloc_test(verbose);

  printf("all tests passed!\n");

  return EXIT_SUCCESS;
}

void fill(int *array, int sz)
{
  unsigned int i;
  for(i = 0; i<(sz/sizeof(int)); i++)
  {
    array[i] = rand() % 50;
  }
}

void show(int *array, int sz)
{
  printf("---------------\n");
  unsigned int i;
  for(i = 0; i<(sz/sizeof(int)); i++)
  {
    printf("%i: %i - ", i, array[i]);
  }
  printf("\n");
}

int test_realloc_main(int argc, char **argv)
{
  int nb = 4;
  int sz = nb * sizeof(int);

  int *array = (int *) acmalloc(sz);
  fill(array, sz);
  // show(array, sz);

  unsigned int i;
  for(i = 1; i<10; i++)
  {
    array = (int *) acrealloc(array, i * sz);
    sz = i*sz;
    // show(array, sz);
    fill(array, sz);
    // show(array, sz);
  }

  acfree(array);

  return 0;
}
